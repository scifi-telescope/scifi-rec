# SciFi Reconstruction

## Dependencies

SciFi Reconstruction requires a C++14 compatible compiler, [CMake][cmake], [Eigen][eigen],
and [ROOT][root] for its core functionality. Optional components might require
additional software. A full list of dependencies and compatible versions
can be found below.

*   A C++14-compatible compiler, e.g. gcc 7 or clang 3.9
*   [CMake][cmake] >=3.7
*   [Eigen][eigen] >=3.2.9
*   [ROOT][root] >=6.10

## Building

Use the following commands to build the software using [CMake][cmake] in a
separate build directory:
```bash
mkdir build
cd build
cmake .. # or, to optimize the code, cmake -DCMAKE_BUILD_TYPE=Release ..
make -jN # (N is the number of cores of your processor)
```
The resulting binaries will be located in `build/bin`. An additional
activation script is provided that updates the paths variables in the shell
environment. By sourcing it via

```bash
source build/activate.sh
```

the `sfr-...` binaries can be called directly without specifying their location explicitly.

## Documentation

TODO

## Authors

SciFi Reconstruction is written and maintained by:

*   Ettore Zaffaroni

SciFi Reconstruction has seen contributions from (in alphabetical order):

*   Carina Trippl

## Citation

TODO zenodo

## History

SciFi Reconstruction is heavily based on [Proteus][proteus-zenodo], written by Moritz Kiehn.

Proteus initially started as a fork of the [Judith][judith-sw] software
written by

*   Garrin McGoldrick
*   Matevž Červ
*   Andrej Gorišek

and described in
[G. McGoldrick et al., NIM A765 140--145, Nov. 2014][judith-paper2014].

## License

The software is distributed under the terms of the
[MIT license][license-mit]. The documentation is distributed under the
terms of the [CC-BY-4.0][license-ccby4] license.  The licenses can be
found in the `LICENSE` file. Contributions are expected to be submitted
under the same license terms.

The software includes a copy of [tinytoml][tinytoml] which is distributed under
the simplified BSD License.

[cmake]: http://www.cmake.org
[eigen]: http://eigen.tuxfamily.org
[judith-sw]: https://github.com/gmcgoldr/judith
[judith-paper2014]: http://dx.doi.org/10.1016/j.nima.2014.05.033
[license-bsd]: https://spdx.org/licenses/BSD-2-Clause.html
[license-ccby4]: https://creativecommons.org/licenses/by/4.0/
[license-mit]: https://spdx.org/licenses/MIT.html
[proteus-zenodo]: https://zenodo.org/record/2579153
[root]: https://root.cern.ch
[tinytoml]: https://github.com/mayah/tinytoml


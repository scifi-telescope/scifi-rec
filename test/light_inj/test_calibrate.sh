#!/bin/bash

run_n=$1; shift

# create folder and calculate calibration
mkdir -p output/calibration_test/run_`printf %06d $run_n`
sfr-calibrate converted_data/run_`printf %06d $run_n`_0.root output/calibration_test/run_`printf %06d $run_n` $@
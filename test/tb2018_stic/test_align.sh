#!/bin/bash

run_n=$1; shift

mkdir -p output/align_test
# align along x
sfr-align converted_data/run_`printf %06d $run_n`.root output/align_test/run_`printf %06d $run_n`_x- -u x $@
# using the new geometry, align along y
sfr-align converted_data/run_`printf %06d $run_n`.root output/align_test/run_`printf %06d $run_n` -u y -g output/align_test/run_`printf %06d $run_n`_x-geo.toml $@

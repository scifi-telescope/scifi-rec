# TEST
Run tests of the various executables. Start with:
```sh
source setup.sh
```
to add the binaries folder to the `PATH`.

The data need to be downloaded manually, for the moment. The scripts are currently designed to work with run 1041 only, this will be expanded in the future. Download the folders `RUN_1041` and `RUN_1041_0` from the server and place them in the `raw_data` folder.

Then, the first thing that needs to be run is `test_convert.sh`, which will convert the raw data to the SciFi1 format (a ROOT file) and place it in `converted_data`.

Then the order is:
```sh
./test_calibrate.sh # this only calculates the calibration, the executable to apply it needs to be added
./test_align.sh
./test_clusterize.sh
```
For the moment the order is not actually important, but once the code to apply the calibration and for tracking will be included, it will become more important (calibration as the first thing, then alignment and then tracking)

## Folder structure
The scripts and initial configuration files are stored in the main folder.
Then the following folders are present:
* `raw_data`, which contains the raw data in folders with names `RUN_XXXX`,  `RUN_XXXX_0` and `RUN_XXXX_1`;
* `converted_data`, which contains the converted data as root files with name `run_XXXXXX.root` and the hits histograms as `run_XXXXXX-hists.root`;
* `output`, which contains one folder for each of the output of the remaining scripts, such as alignemnt, calibration, etc.
* `calibrated_data`, not there yet, but may be added once the code that applies the calibration is ready.

**NB** each of these folders **must** contain a `.gitignore` file containing 

```
*
!.gitignore
```

in order to avoid to put on git all the test results and data, while preserving the directories structure.

## TODOs

* [ ] write a script that downloads the data (maybe chosing the run to be downloaded)
* [ ] make the test scripts work with any run number
* [ ] add calibrated data folder

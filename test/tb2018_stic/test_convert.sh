#!/bin/bash

run_n=$1; shift

# convert raw data, data and calibration both
sfr-convert raw_data/RUN_`printf %04d $run_n` converted_data/run_`printf %06d $run_n` -u data $@
sfr-convert raw_data/RUN_`printf %04d $run_n`_0 converted_data/run_`printf %06d $run_n`_0 -u calibration $@
#!/bin/bash

run_n=$1; shift

# crete folder and run clusterization
mkdir -p output/cluster_test
sfr-clusterize converted_data/run_`printf %06d $run_n`.root output/cluster_test/run_`printf %06d $run_n` $@
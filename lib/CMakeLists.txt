# Copyright (c) 2020 The SciFi reconstruction authors
# SPDX-License-Identifier: MIT

# If you need to manually exclude something from compilation, use this code
# set(sfr_SOURCES
#     utils/application.cpp
#     utils/arguments.cpp
#     utils/config.cpp
#     utils/logger.cpp
# )

configure_file(utils/version.h.in utils/version.h)

# get a list of all cpp and header files
file(GLOB_RECURSE sfr_SOURCES LIST_DIRECTORIES false *.cpp)
file(GLOB_RECURSE sfr_HEADERS LIST_DIRECTORIES false *.h)

# everything in the aforementioned files will be compiled in one big static library
add_library(sfr STATIC ${sfr_SOURCES} ${sfr_HEADERS})

# add the lib directory (i.e. CMAKE_CURRENT_SOURCE_DIR) to the include path
target_include_directories(
  sfr
  SYSTEM PUBLIC ${EIGEN_INCLUDE_DIRS}
  PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} ${ROOT_INCLUDE_DIRS} ${PROJECT_BINARY_DIR}/lib/utils)
target_link_libraries(
    sfr
    PUBLIC ROOT::Hist ROOT::Tree)
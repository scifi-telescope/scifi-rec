// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

/**
 * \file
 * \author Moritz Kiehn (msmk@cern.ch)
 * \date 2016-10
 */

#pragma once

#include "loop/sensorprocessor.h"
#include "utils/definitions.h"

namespace sfr {

class Sensor;

/** Cluster hits and average the position with equal weights for all hits.
 *
 * The fastest hit time is used as the cluster time.
 */
// class BinaryClusterizer : public SensorProcessor {
// public:
//   BinaryClusterizer(const Sensor& sensor) : m_sensor(sensor) {}

//   std::string name() const;
//   void execute(SensorEvent& sensorEvent) const;

// private:
//   const Sensor& m_sensor;
// };

/** Cluster hits and calculate position by weighting each hit with its value.
 *
 * The fastest hit time is used as the cluster time.
 */
class ValueWeightedClusterizer : public SensorProcessor {
public:
  ValueWeightedClusterizer(const Sensor& sensor) : m_sensor(sensor) {}

  std::string name() const;
  void execute(SensorEvent& sensorEvent) const;

private:
  const Sensor& m_sensor;
};

/** Cluster all hits in each event in one cluster and calculate position by weighting each hit with its value.
 *
 * The fastest hit time is used as the cluster time.
 */
class ShowerValueWeightedClusterizer : public SensorProcessor {
public:
  ShowerValueWeightedClusterizer(const Sensor& sensor) : m_sensor(sensor) {}

  std::string name() const;
  void execute(SensorEvent& sensorEvent) const;

private:
  const Sensor& m_sensor;
};

/** Cluster hits and take position and timing only from the fastest hit. */
// class FastestHitClusterizer : public SensorProcessor {
// public:
//   FastestHitClusterizer(const Sensor& sensor) : m_sensor(sensor) {}

//   std::string name() const;
//   void execute(SensorEvent& sensorEvent) const;

// private:
//   const Sensor& m_sensor;
// };

/** Do not cluster. Each hit becomes a cluster. */
class NonClusterizer : public SensorProcessor {
public:
  NonClusterizer(const Sensor& sensor,
                  Scalar channelVarScale = 1.0,
                  Scalar distVarScale = 1.0)
                  : m_sensor(sensor),
                  m_channelVarScale(channelVarScale),
                  m_distVarScale(distVarScale) {}

  std::string name() const;
  void execute(SensorEvent& sensorEvent) const;

private:
  const Sensor& m_sensor;
  const Scalar m_channelVarScale, m_distVarScale;
};

} // namespace sfr

// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

/**
 * \file
 * \author Moritz Kiehn (msmk@cern.ch), C. Trippl (carina.trippl@epfl.ch), E. Zaffaroni (ettore.zaffaroni@epfl.ch)
 * \date 2020-06
 */

#include "hitfilter.h"

//#include <algorithm>
//#include <functional>
//#include <limits>
//#include <set>

#include "loop/eventloop.h"
#include "mechanics/device.h"
#include "storage/sensorevent.h"
//#include "utils/interval.h"
#include "utils/logger.h"

namespace sfr {

std::string SticHitFilter::name() const
{
  return "SticHitFilter(" + m_sensor.name() + ")";
}

void SticHitFilter::execute(SensorEvent& sensorEvent) const
{  
  auto hitsBegin = sensorEvent.m_hits.begin();
  auto hitsEnd = sensorEvent.m_hits.end();

  auto isBad = [] (std::unique_ptr<Hit>& hit) { return hit->fineValue() == 0 || hit->fineValue() >= 255; };

  auto it = std::remove_if(hitsBegin, hitsEnd, isBad);
  sensorEvent.m_hits.erase(it, hitsEnd);
}

} // namespace sfr

// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#pragma once

#include "loop/sensorprocessor.h"
#include "utils/definitions.h"

namespace sfr {

class Sensor;

class SticHitFilter : public SensorProcessor {
public:
  SticHitFilter(const Sensor& sensor) : m_sensor(sensor) {}
  
  std::string name() const;
  void execute(SensorEvent& sensorEvent) const;

private:
  const Sensor& m_sensor;
};

} // namespace sfr

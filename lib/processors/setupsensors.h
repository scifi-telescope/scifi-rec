// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

/** Automated setup of per-sensor processors.
 *
 * \author Moritz Kiehn <msmk@cern.ch>
 * \date 2017-02
 */

#pragma once

namespace sfr {

class Device;
class EventLoop;

/** Add per-sensor processors to the event loop.
 *
 * Depending on the device configuration this can include hit mapping,
 * hit region identification, clustering, and digital-to-local transformation.
 */
void setupPerSensorProcessing(const Device& device, EventLoop& loop);

} // namespace sfr

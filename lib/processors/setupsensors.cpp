// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

/**
 * \author Moritz Kiehn <msmk@cern.ch>
 * \date 2017-02
 */

#include "setupsensors.h"

#include "loop/eventloop.h"
#include "mechanics/device.h"
#include "processors/applylocaltransform.h"
//#include "processors/applyregions.h"
#include "processors/clusterizer.h"
#include "processors/hitfilter.h"
//#include "processors/hitmapper.h"

namespace sfr {

namespace {
void setupSensor(Index sensorId, const Sensor& sensor, EventLoop& loop)
{
  // hit mapper
  // if (sensor.measurement() == Sensor::Measurement::Ccpdv4Binary) {
  //   loop.addSensorProcessor(sensorId, std::make_shared<CCPDv4HitMapper>());
  // }
  // else if (sensor.measurement() == Sensor::Measurement::HexagonalTot) {
  //   loop.addSensorProcessor(sensorId, std::make_shared<AxialHexagonalHitMapper>());
  // }
  // // sensor regions
  // if (sensor.hasRegions()) {
  //   loop.addSensorProcessor(sensorId, std::make_shared<ApplyRegions>(sensor));
  // }
  // clusterizer
  switch (sensor.measurement()) {
  case Sensor::Measurement::SciFiStic:
    loop.addSensorProcessor(sensorId,
                            std::make_shared<SticHitFilter>(sensor));
    loop.addSensorProcessor(sensorId,
                            std::make_shared<ValueWeightedClusterizer>(sensor));
    break;
  case Sensor::Measurement::SciFiSticShower:
    loop.addSensorProcessor(sensorId,
                            std::make_shared<SticHitFilter>(sensor));
    loop.addSensorProcessor(sensorId,
                            std::make_shared<ShowerValueWeightedClusterizer>(sensor));
    break;
  // case Sensor::Measurement::PixelTot:
  //   loop.addSensorProcessor(sensorId,
  //                           std::make_shared<ValueWeightedClusterizer>(sensor));
  //   break;
  // case Sensor::Measurement::HexagonalTot:
  //   loop.addSensorProcessor(sensorId,
  //                           std::make_shared<NonClusterizer>(sensor, 2.0, 1.0));
  //   break;
  }
  // digital-to-local transform
  // if (sensor.measurement() == Sensor::Measurement::HexagonalTot) {
  //   loop.addSensorProcessor(
  //     sensorId, std::make_shared<ApplyLocalTransformHexAxial>(sensor));
  // }
  // else {
    loop.addSensorProcessor(
      sensorId, std::make_shared<ApplyLocalTransformCartesian>(sensor));
  // }
  
}
} // namespace

void setupPerSensorProcessing(const Device& device, EventLoop& loop)
{
  for (auto isensor : device.sensorIds()) {
    setupSensor(isensor, device.getSensor(isensor), loop);
  }
}

} // namespace sfr

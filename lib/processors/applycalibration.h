// Copyright (c) 2014-2019 The Proteus authors
// SPDX-License-Identifier: MIT
/**
 * \file
 * \author Carina Trippl (carina.trippl@epfl.ch)
 * \date 2020-08
 */

#pragma once

#include "loop/sensorprocessor.h"
#include "utils/definitions.h"
#include "mechanics/sensorcalibration.h"

namespace sfr {

class Sensor;

/**
 * Loads calibration from files in the constructor and applies it to sensorEvents inside execute.
 **/

class SticApplyCalibration : public SensorProcessor {
public:
  SticApplyCalibration(const Sensor& sensor, const std::string filename);
  
  std::string name() const;
  void execute(SensorEvent& sensorEvent) const;

  SensorCalibration getSensorCalibration() const { return m_sensorCalibration; }

private:
  const Sensor& m_sensor;
  SensorCalibration m_sensorCalibration;
};

} // namespace sfr

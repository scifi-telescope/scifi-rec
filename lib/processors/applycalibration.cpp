// Copyright (c) 2014-2019 The Proteus authors
// SPDX-License-Identifier: MIT
/*
 * \file
 * \author C. Trippl (carina.trippl@epfl.ch)
 * \date 2020-08
 */

#include "applycalibration.h"
#include "loop/eventloop.h"
#include "mechanics/device.h"
#include "storage/sensorevent.h"
#include "utils/logger.h"

namespace sfr {

SticApplyCalibration::SticApplyCalibration(const Sensor& sensor, const std::string filename) : m_sensor(sensor), m_sensorCalibration(sensor.nChannels()) {
  m_sensorCalibration.fromFile(filename);
}

std::string SticApplyCalibration::name() const
{
  return "SticApplyCalibration(" + m_sensor.name() + ")";
}

void SticApplyCalibration::execute(SensorEvent& sensorEvent) const
{  
  auto hitsBegin = sensorEvent.m_hits.begin();
  auto hitsEnd = sensorEvent.m_hits.end();

  for (auto h=hitsBegin; h!=hitsEnd; h++) {

    Hit& hit = *(h->get());
    
    if (m_sensorCalibration.getChannelCalibration(hit.channel()) > 0) {
      if ( (m_sensorCalibration.getChannelCalibration(hit.channel()) % 32) <= hit.fineTimestamp()){
        hit.m_fineTimestamp -= (m_sensorCalibration.getChannelCalibration(hit.channel()) % 32);
        hit.m_coarseTimestamp -= (m_sensorCalibration.getChannelCalibration(hit.channel()) / 32);
      }
      else if ( (m_sensorCalibration.getChannelCalibration(hit.channel()) % 32) > hit.fineTimestamp()){
        hit.m_fineTimestamp = 32 - (hit.m_fineTimestamp - (m_sensorCalibration.getChannelCalibration(hit.channel()) % 32)); //has to increase again after reaching 0
        hit.m_coarseTimestamp -= (m_sensorCalibration.getChannelCalibration(hit.channel()) / 32) + 1;
      }
    }
    
    else if (m_sensorCalibration.getChannelCalibration(hit.channel()) < 0) {
      if ( -(m_sensorCalibration.getChannelCalibration(hit.channel()) % 32) <=hit.fineTimestamp()) {
        hit.m_fineTimestamp += (m_sensorCalibration.getChannelCalibration(hit.channel()) % 32);
        hit.m_coarseTimestamp -= (m_sensorCalibration.getChannelCalibration(hit.channel()) / 32);
      }
      else {
        hit.m_fineTimestamp = 32 + (hit.m_fineTimestamp + (m_sensorCalibration.getChannelCalibration(hit.channel()) % 32));
        hit.m_coarseTimestamp -= (m_sensorCalibration.getChannelCalibration(hit.channel())/32) + 1;
      }
    }
  }
}

} // namespace sfr

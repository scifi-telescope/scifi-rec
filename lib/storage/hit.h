// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#pragma once

#include <iosfwd>
#include <cstdint>

#include "utils/definitions.h"

namespace sfr {

/** A sensor hit identified by its address, timestamp, and value.
 *
 * To support devices where the recorded hit address does not directly
 * correspond to the pixel address in the physical pixel matrix, e.g. CCPDv4,
 * the Hit has separate digital (readout) and physical (pixel matrix)
 * addresses.
 */
class Hit {
public:
  Hit(int32_t channel, int64_t coarseTimestamp, int64_t fineTimestamp, int64_t coarseValue, int64_t fineValue, uint64_t flags);

  /** Set only the physical address leaving the digital address untouched. */
  //void setPhysicalAddress(int col, int row);
  /** Set the region id. */
  //void setRegion(Index region) { m_region = region; }
  /** Set the cluster index. */
  void setCluster(Index cluster);

  int32_t channel() const { return m_channel; }
  int64_t coarseTimestamp() const { return m_coarseTimestamp; }
  int64_t fineTimestamp() const { return m_fineTimestamp; }
  int64_t timestamp(int ratio) const { return ratio * m_coarseTimestamp + m_fineTimestamp; }
  int64_t coarseValue() const { return m_coarseValue; }
  int64_t fineValue() const { return m_fineValue; }
  int64_t value(int ratio) const { return ratio * m_coarseValue + m_fineValue; }
  uint64_t flags() const { return m_flags; }

  // int64_t triggerTimestamp() const { return m_triggerTimestamp; }
  // uint64_t triggerFlags() const { return m_triggerFlags; }
  

  //bool hasRegion() const { return m_region != kInvalidIndex; }
  //Index region() const { return m_region; }

  bool isInCluster() const { return m_cluster != kInvalidIndex; }
  Index cluster() const { return m_cluster; }

private:
  int32_t m_channel;
  int64_t m_coarseTimestamp;
  int64_t m_fineTimestamp;
  int64_t m_coarseValue;
  int64_t m_fineValue;
  uint64_t m_flags;

  // int64_t m_triggerTimestamp;
  // uint64_t m_triggerFlags;

  Index m_cluster;

  friend class SticApplyCalibration;

};

std::ostream& operator<<(std::ostream& os, const Hit& hit);

} // namespace sfr

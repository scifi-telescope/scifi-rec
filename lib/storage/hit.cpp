// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#include "hit.h"

#include <cassert>
#include <ostream>

//#include "cluster.h"

namespace sfr {

Hit::Hit(int32_t channel, int64_t coarseTimestamp, int64_t fineTimestamp, int64_t coarseValue, int64_t fineValue, uint64_t flags)
    : m_channel(channel)
    , m_coarseTimestamp(coarseTimestamp)
    , m_fineTimestamp(fineTimestamp)
    , m_coarseValue(coarseValue)
    , m_fineValue(fineValue)
    , m_flags(flags)
    , m_cluster(kInvalidIndex)
{
}

// void Hit::setPhysicalAddress(int col, int row)
// {
//   m_col = col;
//   m_row = row;
// }

void Hit::setCluster(Index cluster)
{
  assert((m_cluster == kInvalidIndex) && "hit can only be in one cluster");
  m_cluster = cluster;
}

std::ostream& operator<<(std::ostream& os, const Hit& hit)
{
  // if ((hit.digitalCol() != hit.col()) || (hit.digitalRow() != hit.row())) {
  //   os << "addr0=" << hit.digitalCol();
  //   os << " addr1=" << hit.digitalRow();
  //   os << " ";
  // }
  // os << "col=" << hit.col();
  // os << " row=" << hit.row();
  // os << " ts=" << hit.timestamp();
  // os << " value=" << hit.value();
  // if (hit.hasRegion()) {
  //   os << " region=" << hit.region();
  // }
  // if (hit.isInCluster()) {
  //   os << " cluster=" << hit.cluster();
  // }
  return os;
}

} // namespace sfr

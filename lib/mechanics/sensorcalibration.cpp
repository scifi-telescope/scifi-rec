// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#include "sensorcalibration.h"

#include <fstream>

#include "utils/config.h"
#include "utils/logger.h"

namespace sfr {

SensorCalibration::SensorCalibration(Index nChannels) {
    m_calibration = std::vector<int64_t>(nChannels, 0);
    m_mask = std::vector<uint8_t>(nChannels, 0);
}

void SensorCalibration::fromFile(const std::string& filename) {
  std::ifstream f(filename);

  if (!f) {
    FAIL("Configuration file ", filename, " not found.");
    return;
  }

  int32_t ch;
  int64_t cal;
  uint8_t mask;

  while(f >> ch >> cal >> mask) {
    m_calibration.at(ch) = cal;
    m_mask.at(ch) = mask;
  }
}

void SensorCalibration::writeFile(const std::string& filename) const {
  std::ofstream f(filename);

  if (!f) {
    WARN("Error opening configuration file ", filename);
    return;
  }

  for (Index i = 0; i<m_mask.size(); i++) {
    f << i << "\t" << m_calibration.at(i) << "\t" << static_cast<unsigned int>(m_mask.at(i)) << std::endl;
  }



}

} // namspace sfr
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#pragma once

#include <vector>
#include <string>
#include <stdint.h>

#include "utils/definitions.h"

namespace sfr {

class SensorCalibration {
 public:
  SensorCalibration(Index nChannels);

  inline void setChannelCalibration(const Index channel, const int64_t calibration) {
    m_calibration.at(channel) = calibration;
  }
  inline int64_t getChannelCalibration(const Index channel) const { return m_calibration.at(channel); }

  inline void setChannelMask(const Index channel, const uint8_t mask) {
    m_mask.at(channel) = mask;
  }
  inline uint8_t getChannelMask(const Index channel) const { return m_mask.at(channel); }

  void fromFile(const std::string& filename);
  void writeFile(const std::string& filename) const;

private:
  std::vector<int64_t> m_calibration;
  std::vector<uint8_t> m_mask;

};

} //namespace sfr
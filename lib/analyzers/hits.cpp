// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#include "hits.h"

#include <cassert>
#include <cmath>
#include <sstream>

#include "mechanics/device.h"
#include "storage/event.h"
#include "utils/root.h"
#include "utils/logger.h"

namespace sfr {

SensorHits::SensorHits(TDirectory* dir, const Sensor& sensor) : m_sensor(sensor)
{
  m_sub = makeDir(dir, "sensors/" + sensor.name() + "/hits");
}

void SensorHits::execute(const SensorEvent& sensorEvent)
{
  m_nHits_v.push_back(sensorEvent.numHits());

  for (Index ihit = 0; ihit < sensorEvent.numHits(); ++ihit) {
    const Hit& hit = sensorEvent.getHit(ihit);
    m_channel_v.push_back(hit.channel());
    m_valueCoarse_v.push_back(hit.coarseValue());
    m_valueFine_v.push_back(hit.fineValue());
    m_timestampCoarse_v.push_back(hit.coarseTimestamp());
    m_timestampFine_v.push_back(hit.fineTimestamp());
  }
}

void SensorHits::finalize()
{
  auto smallest_floor = [](const auto v) {
    auto it_min = std::min_element(std::begin(v), std::end(v));
    if (it_min == v.end()) {
      return static_cast<double>(0);
    }
    else {
      return std::floor(*it_min);
    }
  };

  auto largest = [](const auto v) {
    auto it_max = std::max_element(std::begin(v), std::end(v));
    if (it_max == v.end()) {
      return 0;
    }
    else {
      return *it_max;
    }
  };

  auto largest_ceil = [](const auto v) {
    auto it_max = std::max_element(std::begin(v), std::end(v));
    if (it_max == v.end()) {
      return static_cast<double>(0);
    }
    else {
      return std::ceil(*it_max);
    }
  };

  // get histograms ranges from data
  auto axChannel = HistAxis::Integer(0, m_sensor.nChannels(), "Hit channel");
  auto axValueCoarse = HistAxis::Integer(0, largest_ceil(m_valueCoarse_v)+1, "Hit coarse value");
  auto axValueFine = HistAxis::Integer(0, m_sensor.fineValueMax()+1, "Hit fine value");

  // left limit is the smallest element in the vector, rounded down
  // right limit is the largest element in the vector, rounded up
  double axTsLeft = smallest_floor(m_timestampCoarse_v);
  double axTsRight = largest_ceil(m_timestampCoarse_v);
  //needed otherwise a single huge timestamp can create millions or billions of bins
  //TODO set max and min from configuration file
  axTsLeft = std::max(axTsLeft, -1e4);
  axTsRight = std::min(axTsRight, 1e4);
  auto axTsCoarse = HistAxis::Integer(axTsLeft, axTsRight+1, "Hit coarse timestamp");
  auto axTsFine = HistAxis::Integer(0, m_sensor.fineTimestampMax()+1, "Hit fine timestamp");

  // create the histograms
  m_nHits = makeH1(m_sub, "nhits", HistAxis::Integer(0, largest(m_nHits_v)+1, "Hits / event"));
  m_rate = makeH1(m_sub, "rate", HistAxis{0.0, 1.0, 100, "Hits / channel / event"});
  m_channel = makeH1(m_sub, "channel", axChannel);
  m_valueCoarse = makeH1(m_sub, "value_coarse", axValueCoarse);
  m_valueFine = makeH1(m_sub, "value_fine", axValueFine);
  m_timestampCoarse = makeH1(m_sub, "timestamp_coarse", axTsCoarse);
  m_timestampFine = makeH1(m_sub, "timestamp_fine", axTsFine);
  
  m_channelValueCoarse = makeH2(m_sub, "channel__value_coarse", axChannel, axValueCoarse);
  m_channelValueFine = makeH2(m_sub, "channel__value_fine", axChannel, axValueFine);
  m_channelTimestampCoarse = makeH2(m_sub, "channel__timestamp_coarse", axChannel, axTsCoarse);
  m_channelTimestampFine = makeH2(m_sub, "channel__timestamp_fine", axChannel, axTsFine);
  
  // fill the histograms
  for (const auto& nHits : m_nHits_v) {
    m_nHits->Fill(nHits);
  }

  for (std::vector<int>::size_type i = 0; i < m_channel_v.size(); i++) {
    m_channel->Fill(m_channel_v[i]);
    m_valueCoarse->Fill(m_valueCoarse_v[i]);
    m_valueFine->Fill(m_valueFine_v[i]);
    m_timestampCoarse->Fill(m_timestampCoarse_v[i]);
    m_timestampFine->Fill(m_timestampFine_v[i]);

    m_channelValueCoarse->Fill(m_channel_v[i], m_valueCoarse_v[i]);
    m_channelValueFine->Fill(m_channel_v[i], m_valueFine_v[i]);
    m_channelTimestampCoarse->Fill(m_channel_v[i], m_timestampCoarse_v[i]);
    m_channelTimestampFine->Fill(m_channel_v[i], m_timestampFine_v[i]);
  }

  // rescale rate histogram to available range
  auto numEvents = m_nHits->GetEntries();
  auto maxRate = m_channel->GetMaximum() / numEvents;
  // ensure that the highest rate is still within the histogram limits
  m_rate->SetBins(m_rate->GetNbinsX(), 0,
                  std::nextafter(maxRate, std::numeric_limits<double>::max()));
  m_rate->Reset();
  // fill rate
  for (int ix = 1; ix <= m_channel->GetNbinsX(); ++ix) {

    auto count = m_channel->GetBinContent(ix);
    if (0 < count) {
      m_rate->Fill(m_channel->GetBinContent(ix) / numEvents);
    }
  }
}

Hits::Hits(TDirectory* dir, const Device& device)
{
  for (auto isensor : device.sensorIds()) {
    m_sensors.emplace_back(dir, device.getSensor(isensor));
  }
}

std::string Hits::name() const { return "Hits"; }

void Hits::execute(const Event& event)
{
  for (Index isensor = 0; isensor < event.numSensorEvents(); ++isensor) {
    m_sensors[isensor].execute(event.getSensorEvent(isensor));
  }
}

void Hits::finalize()
{
  for (auto& sensor : m_sensors) {
    sensor.finalize();
  }
}

} // namespace sfr

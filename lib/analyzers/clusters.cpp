// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#include "clusters.h"

#include <cassert>
#include <cmath>
#include <sstream>

#include "mechanics/device.h"
#include "storage/event.h"
#include "utils/root.h"
#include "utils/logger.h"

namespace sfr {

SensorClusters::SensorClusters(TDirectory* dir, const Sensor& sensor, const int sizeMax) : m_sensor(sensor)
{
  m_sub = makeDir(dir, "sensors/" + m_sensor.name() + "/clusters");
}

void SensorClusters::execute(const SensorEvent& sensorEvent)
{
  // It fills vectors here so it can determine the histogram axes automatically in the finalize()
  m_nClusters_v.push_back(sensorEvent.numClusters());

  for (Index icluster = 0; icluster < sensorEvent.numClusters(); ++icluster) {
    const Cluster& cluster = sensorEvent.getCluster(icluster);
    m_size_v.push_back(cluster.size());
    m_channel_v.push_back(cluster.channel());
    m_value_v.push_back(cluster.value());
    m_timestamp_v.push_back(cluster.timestamp());
  }
}

void SensorClusters::finalize()
{
  auto smallest_floor = [](const auto v) {
    return std::floor(*std::min_element(std::begin(v), std::end(v)));
  };

  auto largest = [](const auto v) {
    return *std::max_element(std::begin(v), std::end(v));
  };

  auto largest_ceil = [](const auto v) {
    return std::ceil(*std::max_element(std::begin(v), std::end(v)));
  };

  // get the histograms ranges from data
  auto axChannel = HistAxis::Integer(0, m_sensor.nChannels(), "Cluster channel");
  auto axValue = HistAxis::IntegerLimited(0, largest_ceil(m_value_v)+1, 2000, "Cluster value");
  auto axSize = HistAxis::Integer(0, largest(m_size_v)+1, "Cluster size");
  
  // left limit is the smallest element in the vector, rounded down
  // right limit is the largest element in the vector, rounded up
  double axTsLeft = smallest_floor(m_timestamp_v);
  double axTsRight = largest_ceil(m_timestamp_v);
  //needed otherwise a single huge timestamp can create millions or billions of bins
  //TODO set max and min from configuration file
  axTsLeft = std::max(axTsLeft, -1e4);
  axTsRight = std::min(axTsRight, 1e4);
  auto axTs = HistAxis::Integer(axTsLeft, axTsRight, "Cluster timestamp");
  
  // create the histograms
  m_nClusters = makeH1(m_sub, "nclusters", HistAxis::Integer(0, largest(m_nClusters_v)+1, "Clusters / event"));
  m_rate = makeH1(m_sub, "rate", HistAxis{0.0, 1.0, 100, "Clusters / channel / event"});
  m_size = makeH1(m_sub, "size", axSize);
  m_channel = makeH1(m_sub, "channel", axChannel);
  m_value = makeH1(m_sub, "value", axValue);
  m_timestamp = makeH1(m_sub, "timestamp", axTs);
  
  m_channelSize = makeH2(m_sub, "channel_size", axChannel, axSize);
  m_channelValue = makeH2(m_sub, "channel_value", axChannel, axValue);
  m_channelTimestamp = makeH2(m_sub, "channel_timestamp", axChannel, axTs);
  m_sizeValue = makeH2(m_sub, "size_value", axSize, axValue);
  m_sizeTimestamp = makeH2(m_sub, "size_timestamp", axSize, axTs);
  m_valueTimestamp = makeH2(m_sub, "value_timestamp", axValue, axTs);

  // fill the histograms
  for (const auto& nClusters : m_nClusters_v) {
    m_nClusters->Fill(nClusters);
  }

  for (std::vector<int>::size_type i = 0; i < m_size_v.size(); i++) {
    m_size->Fill(m_size_v[i]);
    m_channel->Fill(m_channel_v[i]);
    m_value->Fill(m_value_v[i]);
    m_timestamp->Fill(m_timestamp_v[i]);

    m_channelSize->Fill(m_channel_v[i], m_size_v[i]);
    m_channelValue->Fill(m_channel_v[i], m_value_v[i]);
    m_channelTimestamp->Fill(m_channel_v[i], m_timestamp_v[i]);
    m_sizeValue->Fill(m_size_v[i], m_value_v[i]);
    m_sizeTimestamp->Fill(m_size_v[i], m_timestamp_v[i]);
    m_valueTimestamp->Fill(m_value_v[i], m_timestamp_v[i]);
  }

  // rescale rate histogram to available range
  auto numEvents = m_nClusters->GetEntries();
  auto maxRate = m_channel->GetMaximum() / numEvents;
  // ensure that the highest rate is still within the histogram limits
  m_rate->SetBins(m_rate->GetNbinsX(), 0,
                  std::nextafter(maxRate, std::numeric_limits<double>::max()));
  m_rate->Reset();
  // fill rate
  for (int ix = 1; ix <= m_channel->GetNbinsX(); ++ix) {
    auto count = m_channel->GetBinContent(ix);
    if (0 < count) {
      m_rate->Fill(m_channel->GetBinContent(ix) / numEvents);
    }
  }
}

Clusters::Clusters(TDirectory* dir, const Device& device, const int sizeMax)
{
  for (auto isensor : device.sensorIds()) {
    m_sensors.emplace_back(dir, device.getSensor(isensor), sizeMax);
  }
}

std::string Clusters::name() const { return "Clusters"; }

void Clusters::execute(const Event& event)
{
  for (Index isensor = 0; isensor < event.numSensorEvents(); ++isensor) {
    m_sensors[isensor].execute(event.getSensorEvent(isensor));
  }
}

void Clusters::finalize()
{
  for (auto& sensor : m_sensors) {
    sensor.finalize();
  }
}

} // namespace sfr

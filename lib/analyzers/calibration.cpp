// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#include "calibration.h"
#include "utils/logger.h"

namespace sfr {

static int64_t calculateSensorCalibration(TH2D* h2, Index channel, Scalar channelCoefficient) {
  auto* hProj = h2->ProjectionX("hProj", channel+1, channel+1);
  int maxBin = hProj->GetMaximumBin(); 
  int chosenBin = 0;

  for(int i=0; i<maxBin; i++){
    if(hProj->GetBinContent(i) >= 0.05 * hProj->GetBinContent(maxBin)){
        if(hProj->GetBinContent(i+1) > 0 && hProj->GetBinContent(i+2) > 0){
            chosenBin = i;
            break;
        }
    }
  }

    return std::llround(hProj->GetBinCenter(chosenBin) + channel * channelCoefficient);
}

SticCalibration::SticCalibration(TDirectory* dir, const Sensor& sensor, const Scalar channelCoefficient) 
: m_sensor(sensor)
, m_sensorCalibration(sensor.nChannels())
, m_channelCoefficient(channelCoefficient)
{
  // HistAxis axShift(
  //   -sensor.pitchFineTimestamp() * sensor.fineTimestampMax() * 10,
  //   sensor.pitchFineTimestamp() * sensor.fineTimestampMax() * 10,
  //   sensor.fineTimestampMax() * 20, 
  //   "Time shift");
  
  HistAxis axShift = HistAxis::Integer(-sensor.fineTimestampMax() * 10, sensor.fineTimestampMax() * 10, "Time shift");
  HistAxis axChannel = HistAxis::Integer(0, sensor.nChannels(), "Channel");

  TDirectory* sub = makeDir(dir, "sensors/" + sensor.name() + "/calibration");
  m_shifts = makeH2(sub, "time_shift", axShift, axChannel);
}

void SticCalibration::execute(const Event& event) {
  const SensorEvent& sensorEvent = event.getSensorEvent(m_sensor.id());
  //Scalar triggerTime = m_sensor.getRealTimestamp(0, event.timestamp());

  for (Index i = 0; i < sensorEvent.numHits(); ++i) {
    const Hit& hit = sensorEvent.getHit(i);
    //Scalar hitTime = m_sensor.getRealTimestamp(hit.fineTimestamp(), hit.coarseTimestamp());
    //m_shifts->Fill(hitTime - triggerTime, hit.channel());
    int64_t hitTime = hit.timestamp(std::lround(m_sensor.pitchCoarseTimestamp()/m_sensor.pitchFineTimestamp()));
    m_shifts->Fill(hitTime, hit.channel());

  }
  m_numEvents++;
}

void SticCalibration::finalize() {
  for (Index i = 0; i < m_sensor.nChannels(); i++) {
    m_sensorCalibration.setChannelCalibration(i, calculateSensorCalibration(m_shifts, i, m_channelCoefficient)
    //std::llround(calculateSensorCalibration(m_shifts, i, m_channelCoefficient) / m_sensor.pitchFineTimestamp())
    );
  }
}

std::string SticCalibration::name() const {
  return "SticCalibration(sensorId=" + std::to_string(m_sensor.id()) + ')';
}

}
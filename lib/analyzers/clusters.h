// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#pragma once

#include <vector>

#include <TDirectory.h>
#include <TH1D.h>
#include <TH2D.h>

#include "loop/analyzer.h"

namespace sfr {

class Device;
class Sensor;
class SensorEvent;

/** Hit histograms for a single sensor. */
class SensorClusters {
public:
  SensorClusters(TDirectory* dir, const Sensor& sensor, const int sizeMax);

  void execute(const SensorEvent& sensorEvent);
  void finalize();

private:
  const Sensor& m_sensor;

  TDirectory* m_sub;
  
  TH1D* m_nClusters;
  TH1D* m_rate;

  TH1D* m_size;
  TH1D* m_channel;
  TH1D* m_timestamp;
  TH1D* m_value;

  TH2D* m_channelSize;
  TH2D* m_channelValue;
  TH2D* m_channelTimestamp;
  TH2D* m_sizeValue;
  TH2D* m_sizeTimestamp;
  TH2D* m_valueTimestamp;

  std::vector<int> m_nClusters_v;
  std::vector<int> m_size_v;
  std::vector<double> m_channel_v;
  std::vector<double> m_timestamp_v;
  std::vector<double> m_value_v;
};

/** Hit histograms for all sensors in the device. */
class Clusters : public Analyzer {
public:
  Clusters(TDirectory* dir, const Device& device, const int sizeMax = 9);

  std::string name() const;
  void execute(const Event& event);
  void finalize();

private:
  std::vector<SensorClusters> m_sensors;
};

} // namespace sfr

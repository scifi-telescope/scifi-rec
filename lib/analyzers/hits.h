// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#pragma once

#include <vector>

#include <TDirectory.h>
#include <TH1D.h>
#include <TH2D.h>

#include "loop/analyzer.h"

namespace sfr {

class Device;
class Sensor;
class SensorEvent;

/** Hit histograms for a single sensor. */
class SensorHits {
public:
  SensorHits(TDirectory* dir, const Sensor& sensor);

  void execute(const SensorEvent& sensorEvent);
  void finalize();

private:
  const Sensor& m_sensor;

  TDirectory* m_sub;

  TH1D* m_nHits;
  TH1D* m_rate;
  TH1D* m_channel;
  TH1D* m_valueCoarse;
  TH1D* m_valueFine;
  TH1D* m_timestampCoarse;
  TH1D* m_timestampFine;
  TH2D* m_channelValueCoarse;
  TH2D* m_channelValueFine;
  TH2D* m_channelTimestampCoarse;
  TH2D* m_channelTimestampFine;

  std::vector<int> m_nHits_v;
  std::vector<int32_t> m_channel_v;
  std::vector<int64_t> m_timestampCoarse_v;
  std::vector<int64_t> m_timestampFine_v;
  std::vector<int64_t> m_valueCoarse_v;
  std::vector<int64_t> m_valueFine_v;

};

/** Hit histograms for all sensors in the device. */
class Hits : public Analyzer {
public:
  Hits(TDirectory* dir, const Device& device);

  std::string name() const;
  void execute(const Event& event);
  void finalize();

private:
  std::vector<SensorHits> m_sensors;
};

} // namespace sfr

// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#pragma once

#include <string>

#include "loop/analyzer.h"
#include "mechanics/sensor.h"
#include "mechanics/sensorcalibration.h"
#include "storage/event.h"
#include "utils/root.h"

#include "TH2D.h"
#include "TDirectory.h"

namespace sfr {
class SticCalibration : public Analyzer {
public:
  SticCalibration(TDirectory* dir, const Sensor& sensor, const Scalar channelCoefficient);

  std::string name() const;
  void execute(const Event& event);
  void finalize();

  SensorCalibration getSensorCalibration() const { return m_sensorCalibration; }
  Index getSensorId() const { return m_sensor.id(); }

private:
  uint64_t m_numEvents;

  const Sensor& m_sensor;
  SensorCalibration m_sensorCalibration;
  TH2D* m_shifts;
  const Scalar m_channelCoefficient;
};

} //namespace sfr
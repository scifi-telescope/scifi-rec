// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include <TTree.h>

#include "loop/reader.h"
#include "loop/writer.h"
#include "utils/definitions.h"
#include "utils/root.h"

namespace toml {
class Value;
}
namespace sfr {

/** Common data for RceRoot{Reader,Writer}. */
class SciFi1Common {
protected:
  SciFi1Common(RootFilePtr&& file);
  ~SciFi1Common() = default;

  /* NOTE: these sizes are used to initialize arrays of track, cluster and
   * hit information. BUT these arrays are generated ONLY ONCE and re-used
   * to load events. Vectors could have been used in the ROOT file format, but
   * they would need to be constructed at each event reading step. */
  static constexpr Index kMaxHits = 1 << 14;
  static constexpr Index kMaxTracks = 1 << 14;

  struct SensorTrees {
    TTree* hits = nullptr;
    TTree* clusters = nullptr;
    TTree* intercepts = nullptr;
    int64_t entries = 0;
  };

  RootFilePtr m_file;
  int64_t m_entries;
  int64_t m_next;
  // Trees global to the entire event
  TTree* m_eventInfo;
  TTree* m_tracks;
  // Trees containing event-by-event data for each sensors
  std::vector<SensorTrees> m_sensors;

  // global event info
  ULong64_t triggerTimestamp;
  ULong64_t triggerFlags;
  //Bool_t invalid;
  // global track state and info
  Int_t numTracks;
  Double_t trackChi2[kMaxTracks];
  Int_t trackDof[kMaxTracks];
  Double_t trackX[kMaxTracks];
  Double_t trackY[kMaxTracks];
  Double_t trackSlopeX[kMaxTracks];
  Double_t trackSlopeY[kMaxTracks];
  Double_t trackCov[kMaxTracks][10];
  // local hits
  Int_t numHits;
  Int_t hitChannel[kMaxHits];
  Long64_t hitTimestampCoarse[kMaxHits];
  Long64_t hitTimestampFine[kMaxHits];
  Long64_t hitValueCoarse[kMaxHits];
  Long64_t hitValueFine[kMaxHits];
  Int_t hitInCluster[kMaxHits];
  // local clusters
  Int_t numClusters;
  Double_t clusterChannel[kMaxHits];
  Double_t clusterDist[kMaxHits];
  Double_t clusterVarChannel[kMaxHits];
  Double_t clusterVarDist[kMaxHits];
  Double_t clusterCovChannelDist[kMaxHits];
  Double_t clusterTimestamp[kMaxHits];
  Double_t clusterTimestampVar[kMaxHits];
  Double_t clusterValue[kMaxHits];
  Double_t clusterValueVar[kMaxHits];
  Int_t clusterTrack[kMaxHits];
  // local track states
  Int_t numIntercepts;
  Double_t interceptU[kMaxTracks];
  Double_t interceptV[kMaxTracks];
  Double_t interceptSlopeU[kMaxTracks];
  Double_t interceptSlopeV[kMaxTracks];
  Double_t interceptCov[kMaxTracks][10];
  Int_t interceptTrack[kMaxTracks];
};

/** Read events from a SciFi ROOT file. */
class SciFi1Reader : public SciFi1Common, public Reader {
public:
  /** Return a score of how likely the given path is an RCE Root file. */
  static int check(const std::string& path, const toml::Value& cfg);
  /** Open the the file. */
  static std::shared_ptr<SciFi1Reader> open(const std::string& path,
                                             const toml::Value& /* unused */);

  /** Open an existing file and determine the number of sensors and events. */
  SciFi1Reader(const std::string& path);

  std::string name() const override final;
  uint64_t numEvents() const override final;
  size_t numSensors() const override final;

  void skip(uint64_t n) override final;
  bool read(Event& event) override final;

private:
  int64_t addSensor(TDirectory* dir);
};

/** Write event in the RCE ROOT file format. */
class SciFi1Writer : public SciFi1Common, public Writer {
public:
  /** Open a new file and truncate existing content. */
  SciFi1Writer(const std::string& path, size_t numSensors);
  ~SciFi1Writer();

  std::string name() const;

  void append(const Event& event);

private:
  void addSensor(TDirectory* dir);
};

} // namespace sfr

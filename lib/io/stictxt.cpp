// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#include "stictxt.h"

#include <cassert>
#include <fstream>
#include <algorithm>
#include <deque>

#include <sys/stat.h>

#include "Compression.h"

#include "storage/event.h"
#include "utils/logger.h"
#include "utils/definitions.h"
#include "utils/config.h"

namespace sfr
{

uint32_t decode_channel_stic(uint8_t stic_channel, uint8_t stic_id)
{
  return stic_id / 2 * 128 + stic_channel * 2 + stic_id % 2;
}

// -----------------------------------------------------------------------------
// reader
int SticTxtReader::check(const std::string &path, const toml::Value &cfg)
{
  struct stat info;
  int score = 0;

  VERBOSE("SticTxtReader check");

  if (stat(path.c_str(), &info) != 0)
  {
    return 0;
  }
  else if (info.st_mode & S_IFDIR)
  {
    VERBOSE(path + " is a folder.");
    score += 50;
  }
  else
  {
    return 0;
  }

  std::string filename_template = cfg.get<std::string>("data_filename_template");
  auto ids = cfg.get<std::vector<Index>>("file_ids");
  for (auto id : ids) {
    std::string filename = path + "/" + stringFormat(filename_template, id);

    struct stat info_file;
    if (stat(filename.c_str(), &info_file) != 0)
    {
      return 0;
    }
    else if (info_file.st_mode & S_IFREG)
    {
      VERBOSE(filename + " is a regular file.");
      score += 50;
    }
    else
    {
      return 0;
    }
  }

  return score;
}

std::shared_ptr<SticTxtReader>
SticTxtReader::open(const std::string &path,
                    const toml::Value &cfg)
{
  return std::make_shared<SticTxtReader>(path, cfg);
}

SticTxtReader::SticTxtReader(const std::string &path, const toml::Value &cfg)
: m_next(0)
{
  const std::string filenameTemplate = cfg.get<std::string>("data_filename_template");
  const bool splitEvents = cfg.get<bool>("split_events");
  const auto ids = cfg.get<std::vector<Index>>("file_ids");

  const size_t nSensors = ids.size();

  std::vector<std::deque<SensorEvent>> eventArray(nSensors);

  for (size_t i = 0; i<nSensors; i++)
  {
    std::string filename = path + "/" + stringFormat(filenameTemplate, ids.at(i));
    VERBOSE("Opening " + filename);
    std::ifstream f(filename);
    if (!f) {
      FAIL("File " + filename + " does not exist.");
    }

    uint64_t in_word;

    uint16_t pkt_size = 0;
    // uint8_t pkt_flags = 0;
    uint16_t pkt_count = 0;
    uint64_t m_trig_time = 0;

    while (f >> std::hex >> in_word)
    {
      if (((0xFFFFFFL << 40) & in_word) == (0x00CDEFL << 40))
      { // header
        if (pkt_size != 0)
        {
          if (splitEvents) {
            VERBOSE("Wrong packet length in packet ", pkt_count, " of file ", filename, ". ", pkt_size, " != 0.");
          }
          else {
            WARN("Wrong packet length in packet ", pkt_count, " of file ", filename, ". ", pkt_size, " != 0.");
          }
          
        }
        pkt_size = in_word & 0xFFFF;
        // pkt_flags = (in_word & 0xFF0000) >> 16; //TODO useful?
        pkt_count = (in_word & 0xFFFF000000) >> 24;

        pkt_size--;
      }
      else if (((0xFL << 60) & in_word) == (uint64_t) (0xFL << 60))
      { //trigger
        m_trig_time = (in_word & 0x3FFFFFFFF) << 2; // why the <<2??
        // uint64_t m_trig_count = (in_word & (0x3FFFFFFL << 34)) >> 34;

        eventArray.at(i).push_back(SensorEvent(1L, m_trig_time));
        pkt_size--;
      }
      else if (in_word != 0)
      { // hit
        uint8_t m_hit_fine_time = in_word & 0x1F;
        uint64_t m_hit_coarse_time = (in_word & (0xFFFFFFFFFL << 5)) >> 5; // time_544
        m_hit_coarse_time -= m_trig_time;
        uint8_t m_hit_amplitude = (in_word & (0xFFL << 41)) >> 41;
        uint8_t m_hit_stic_channel = (in_word & (0x3FL << 49)) >> 49;
        uint8_t m_hit_stic_id = (in_word & (0x7L << 55)) >> 55;
        // uint8_t m_hit_board_id = (in_word & (0x3FL << 58)) >> 58; // 1 to 12, shouldn't be needed

        int32_t m_hit_channel = decode_channel_stic(m_hit_stic_channel, m_hit_stic_id);

        if (eventArray.at(i).size() > 0) {
          eventArray.at(i).back().addHit(m_hit_channel, m_hit_coarse_time, m_hit_fine_time, 0L, m_hit_amplitude, 0L);
        }
        else {
          WARN("Found hit before trigger word. Dropping it.");
        }

        pkt_size--;
      }
      
    }

    f.close();
  }

  auto compareSize = [] (const auto& a, const auto& b) {
    return a.size() < b.size();
  };

  // finds out the maximum and minimum number of events for eache sensor
  size_t nMax = (*std::max_element(eventArray.begin(), eventArray.end(), compareSize)).size();
  size_t nMin = (*std::min_element(eventArray.begin(), eventArray.end(), compareSize)).size();

  if (!splitEvents) {
    INFO("Number of events:");
    for (size_t j = 0; j < nSensors; j++) {
      INFO("\tSensor ", j, ": ", eventArray.at(j).size());
    }

    INFO("Verifying timestamp consistency");
    // fo each event...
    for (uint64_t i = 0; i < nMin; i++) {
      // ...store the timestamps in a vector
      std::vector<uint64_t> timestamps(nSensors);
      for (size_t j = 0; j < nSensors; j++) {
        timestamps.at(j) = eventArray.at(j).at(i).timestamp();
      }

      // get the min and max of this vector
      auto minTs = *std::min_element(timestamps.begin(), timestamps.end());
      auto maxTs = *std::max_element(timestamps.begin(), timestamps.end());

      // if they are different, there in an inconsistency
      if (minTs != maxTs) {
        VERBOSE("Fixing timestamp inconsistency for event ", i, " - min ts: ", minTs, ", max ts: ", maxTs);
        // loop over the sensors and remove the timestamps that are smaller than the maximum one
        for (size_t j = 0; j < nSensors; j++) {
          if (timestamps.at(j) < maxTs) {
            // put an iterator to the current event
            auto it1 = eventArray.at(j).begin();
            std::advance(it1, i);

            // find the first event with a timestamp not smaller that the maximum (it2 points to that)
            auto hasLargerTimestamp = [maxTs] (const SensorEvent& sensorEvent) {
              return sensorEvent.timestamp() >= maxTs;
            };
            auto it2 = std::find_if(it1, eventArray.at(j).end(), hasLargerTimestamp);
            if (it2 == eventArray.at(j).end()) {
              ERROR("larger TS not found for sensor ", j);
              //TODO this requires to stop, probably
            }
            // erase all elements between it1 and it2
            VERBOSE("Deleting ", std::distance(it1, it2), " elements from sensor ", j, " - remaining: ", std::distance(it1, eventArray.at(j).end()));
            eventArray.at(j).erase(it1, it2);
          }
        }
        // go back by one: we are deleting elements with ts < than maxTs, so we need to check again
        i--;
        // update nMax and nMin
        nMax = (*std::max_element(eventArray.begin(), eventArray.end(), compareSize)).size();
        nMin = (*std::min_element(eventArray.begin(), eventArray.end(), compareSize)).size();
        VERBOSE("Updated maximum number of events: ", nMax);
        VERBOSE("Updated minimum number of events: ", nMin);
      }
    }
    // at this point the first nMin events have consistent timestamps and if nmin!=nMax is because one or more sensors are missing events at the end
    // for this reason the sensor with the fewer events determines the final number
    INFO("Final number of events: ", nMin);
  }

  INFO("Building events");
  if (!splitEvents) {
    // this is the caes for data files, where the n-th sensorEvent from each sensor composes the n-th event
    for (uint64_t i = 0; i < nMin; i++) {
      m_events.push_back(Event(nSensors));
      m_events.back().clear(eventArray.front().at(i).flags(), eventArray.front().at(i).timestamp());
      for (size_t j = 0; j < nSensors; j++) {
        m_events.back().setSensorData(j, std::move(eventArray.at(j).at(i)));
      }
    }
  }
  else {
    // this is the case for calibration files, where each sensorEvent is saved in an event of its own
    for (size_t j = 0; j < nSensors; j++) {
      for (uint64_t i = 0; i < eventArray.at(j).size(); i++) {
        m_events.push_back(Event(nSensors));
        m_events.back().clear(eventArray.at(j).at(i).flags(), eventArray.at(j).at(i).timestamp());
        m_events.back().setSensorData(j, std::move(eventArray.at(j).at(i)));
      }
    }
  }
  
  m_entries = m_events.size();
}

std::string SticTxtReader::name() const { return "SticTxtReader"; }

uint64_t SticTxtReader::numEvents() const
{
  return static_cast<uint64_t>(m_events.size());
}

size_t SticTxtReader::numSensors() const { return m_events.front().numSensorEvents(); }

void SticTxtReader::skip(uint64_t n)
{
  if (m_entries <= static_cast<int64_t>(m_next + n)) {
    WARN("skipping ", n, " events goes beyond available events");
    m_next = m_entries;
  } else {
    m_next += n;
  }
}

bool SticTxtReader::read(Event &event)
{
  if (m_entries <= m_next)
    return false;
  
  int64_t ievent = m_next++;
  event.clear(m_events.at(ievent).flags(), m_events.at(ievent).timestamp());
  for (Index i = 0; i < m_events.at(ievent).numSensorEvents(); i++)
    event.setSensorData(i, std::move(m_events.at(ievent).getSensorEvent(i)));

  return true;
}

} // namespace sfr

// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

/**
 * \author  Moritz Kiehn <msmk@cern.ch>
 * \date    2018-07
 */

#pragma once

#include <memory>
#include <string>

#include "loop/reader.h"

namespace toml {
class Value;
}
namespace sfr {

/** Open an event file with automatic determination of the file type.
 *
 * \param path  Path to the file to be opened
 * \param cfg   Configuration that will be passed to the reader
 */
std::shared_ptr<Reader> openRead(const std::string& path,
                                 const toml::Value& cfg);

} // namespace sfr

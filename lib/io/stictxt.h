// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include <TTree.h>

#include "loop/reader.h"
#include "loop/writer.h"
#include "utils/definitions.h"
#include "utils/root.h"
#include "utils/config.h"

namespace toml {
class Value;
}
namespace sfr {

/** Read events from a Stic raw TXT file. */
class SticTxtReader : public Reader {
public:/** Return a score of how likely the given path is an RCE Root file. */
  static int check(const std::string& path, const toml::Value& cfg);
  /** Open the the file. */
  static std::shared_ptr<SticTxtReader> open(const std::string& path,
                                             const toml::Value& cfg);
  /** Open an existing path. */
  SticTxtReader(const std::string& path, const toml::Value& cfg);

  std::string name() const override final;
  uint64_t numEvents() const override final;
  size_t numSensors() const override final;

  void skip(uint64_t n) override final;
  bool read(Event& event) override final;

private:
  std::vector<Event> m_events;
  int64_t m_entries;
  int64_t m_next;
  
};

} // namespace sfr

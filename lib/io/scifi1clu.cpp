// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#include "scifi1clu.h"

#include <cassert>

#include "Compression.h"

#include "storage/event.h"
#include "utils/logger.h"
#include "mechanics/device.h"

namespace sfr {

SciFi1CluWriter::SciFi1CluWriter(const std::string& path, const Device& device)
  : m_file(openRootWrite(path))
  , m_entries(0)
  , m_next(0)
  , m_eventInfo(nullptr)
  , m_dev(device)
  // , m_tracks(nullptr)
{
  m_file->cd();

  // global event tree
  m_eventInfo = new TTree("event", "Event information");
  m_eventInfo->SetDirectory(m_file.get());
  m_eventInfo->Branch("timestamp", &triggerTimestamp, "Timestamp/l");
  m_eventInfo->Branch("flags", &triggerFlags, "Flags/l");

  // global track tree
  // m_tracks = new TTree("tracks", "Track parameters");
  // m_tracks->SetDirectory(m_file.get());
  // m_tracks->Branch("n_tracks", &numTracks, "NTracks/I");
  // m_tracks->Branch("chi2", trackChi2, "Chi2[NTracks]/D");
  // m_tracks->Branch("dof", trackDof, "Dof[NTracks]/I");
  // m_tracks->Branch("x", trackX, "X[NTracks]/D");
  // m_tracks->Branch("y", trackY, "Y[NTracks]/D");
  // m_tracks->Branch("slope_x", trackSlopeX, "SlopeX[NTracks]/D");
  // m_tracks->Branch("slope_y", trackSlopeY, "SlopeY[NTracks]/D");
  // m_tracks->Branch("cov", trackCov, "Cov[NTracks][10]/D");

  // per-sensor trees
  for (size_t isensor = 0; isensor < m_dev.numSensors(); ++isensor) {
    std::string name("sensor_" + std::to_string(isensor));
    TDirectory* sensorDir = m_file->mkdir(name.c_str());
    addSensor(sensorDir);
  }
}

void SciFi1CluWriter::addSensor(TDirectory* dir)
{
  dir->cd();

  SensorTrees trees;
  // local hits
  trees.hits = new TTree("hits", "hits");
  trees.hits->SetDirectory(dir);
  trees.hits->Branch("n_hits", &numHits, "NHits/I");
  trees.hits->Branch("channel", hitChannel, "Channel[NHits]/I");
  trees.hits->Branch("timestamp_coarse", hitTimestampCoarse, "HitTimestampCoarse[NHits]/L");
  trees.hits->Branch("timestamp_fine", hitTimestampFine, "HitTimestampFine[NHits]/L");
  trees.hits->Branch("value_coarse", hitValueCoarse, "HitValueCoarse[NHits]/L");
  trees.hits->Branch("value_fine", hitValueFine, "HitValueFine[NHits]/L");
  trees.hits->Branch("hit_in_cluster", hitInCluster, "HitInCluster[NHits]/I");
  // local clusters
  trees.clusters = new TTree("clusters", "Clusters");
  trees.clusters->SetDirectory(dir);
  trees.clusters->Branch("n_clusters", &numClusters, "NClusters/I");
  trees.clusters->Branch("x", clusterX, "X[NClusters]/D");
  trees.clusters->Branch("y", clusterY, "Y[NClusters]/D");
  trees.clusters->Branch("z", clusterZ, "Z[NClusters]/D");
  trees.clusters->Branch("time", clusterTime, "Time[NClusters]/D");
  trees.clusters->Branch("std_x", clusterStdX, "StdX[NClusters]/D");
  trees.clusters->Branch("std_y", clusterStdY, "StdY[NClusters]/D");
  trees.clusters->Branch("std_z", clusterStdZ, "StdZ[NClusters]/D");
  trees.clusters->Branch("std_time", clusterStdTime, "StdTime[NClusters]/D");
  trees.clusters->Branch("channel", clusterChannel, "Channel[NClusters]/D");
  trees.clusters->Branch("var_channel", clusterVarChannel, "VarChannel[NClusters]/D");
  trees.clusters->Branch("dist", clusterDist, "Dist[NClusters]/D");
  trees.clusters->Branch("var_dist", clusterVarDist, "VarDist[NClusters]/D");
  trees.clusters->Branch("cov_channel_dist", clusterCovChannelDist,
                         "CovChannelDist[NClusters]/D");
  trees.clusters->Branch("timestamp", clusterTimestamp, "Timestamp[NClusters]/D");
  trees.clusters->Branch("timestamp_var", clusterTimestampVar, "TimestampVar[NClusters]/D");
  trees.clusters->Branch("value", clusterValue, "Value[NClusters]/D");
  trees.clusters->Branch("value_var", clusterValueVar, "ValueVar[NClusters]/D");
  trees.clusters->Branch("track", clusterTrack, "Track[NClusters]/I");
  // local track states
  // trees.intercepts = new TTree("intercepts", "Intercepts");
  // trees.intercepts->SetDirectory(dir);
  // trees.intercepts->Branch("n_intercepts", &numIntercepts, "NIntercepts/I");
  // trees.intercepts->Branch("u", interceptU, "U[NIntercepts]/D");
  // trees.intercepts->Branch("v", interceptV, "V[NIntercepts]/D");
  // trees.intercepts->Branch("slope_u", interceptSlopeU, "SlopeU[NIntercepts]/D");
  // trees.intercepts->Branch("slope_v", interceptSlopeV, "SlopeV[NIntercepts]/D");
  // trees.intercepts->Branch("cov", interceptCov, "Cov[NIntercepts][10]/D");
  // trees.intercepts->Branch("track", interceptTrack, "Track[NIntercepts]/I");
  m_sensors.emplace_back(trees);
}

SciFi1CluWriter::~SciFi1CluWriter()
{
  if (m_file) {
    INFO("wrote ", m_sensors.size(), " sensors to '", m_file->GetPath(), "'");
  }
}

std::string SciFi1CluWriter::name() const { return "SciFi1CluWriter"; }

void SciFi1CluWriter::append(const Event& event)
{
  if (event.numSensorEvents() != m_sensors.size())
    FAIL("inconsistent sensors numbers. events has ", event.numSensorEvents(),
         ", but the writer expected ", m_sensors.size());

  // global event info is **always** filled
  triggerTimestamp = event.timestamp();
  triggerFlags = event.flags();
  m_eventInfo->Fill();

  // tracks
  // if (m_tracks) {
  //   if (kMaxTracks < event.numTracks())
  //     FAIL("tracks exceed MAX_TRACKS");
  //   numTracks = event.numTracks();
  //   for (Index itrack = 0; itrack < event.numTracks(); ++itrack) {
  //     const Track& track = event.getTrack(itrack);
  //     trackChi2[itrack] = track.chi2();
  //     trackDof[itrack] = track.degreesOfFreedom();
  //     const TrackState& state = track.globalState();
  //     trackX[itrack] = state.loc0();
  //     trackY[itrack] = state.loc1();
  //     trackSlopeX[itrack] = state.slopeLoc0();
  //     trackSlopeY[itrack] = state.slopeLoc1();
  //     state.getCovSpatialPacked(trackCov[itrack]);
  //   }
  //   m_tracks->Fill();
  // }

  // per-sensor data
  for (Index isensor = 0; isensor < event.numSensorEvents(); ++isensor) {
    SensorTrees& trees = m_sensors[isensor];
    const auto& sensorEvent = event.getSensorEvent(isensor);

    // local hits
    if (trees.hits) {
      if (kMaxHits < sensorEvent.numHits())
        FAIL("hits exceed MAX_HITS");
      numHits = sensorEvent.numHits();
      for (Index ihit = 0; ihit < sensorEvent.numHits(); ++ihit) {
        const Hit hit = sensorEvent.getHit(ihit);
        hitChannel[ihit] = hit.channel();
        hitTimestampCoarse[ihit] = hit.coarseTimestamp();
        hitTimestampFine[ihit] = hit.fineTimestamp();
        hitValueCoarse[ihit] = hit.coarseValue();
        hitValueFine[ihit] = hit.fineValue();
        hitInCluster[ihit] = hit.isInCluster() ? hit.cluster() : -1;
      }
      trees.hits->Fill();
    }

    // local clusters
    if (trees.clusters) {
      if (kMaxHits < sensorEvent.numClusters())
        FAIL("clusters exceed MAX_HITS");
      numClusters = sensorEvent.numClusters();
      for (Index iclu = 0; iclu < sensorEvent.numClusters(); ++iclu) {
        const Cluster& cluster = sensorEvent.getCluster(iclu);
        Vector4 global = m_dev.geometry().getPlane(isensor).toGlobal(cluster.position());
        clusterX[iclu] = global[kX];
        clusterY[iclu] = global[kY];
        clusterZ[iclu] = global[kZ];
        clusterTime[iclu] = global[kT];
        
        Vector4 stds = cluster.positionCov().diagonal().array().sqrt();
        clusterStdX[iclu] = stds[kX];
        clusterStdY[iclu] = stds[kY];
        clusterStdZ[iclu] = stds[kZ];
        clusterStdTime[iclu] = stds[kTime];

        clusterChannel[iclu] = cluster.channel();
        clusterVarChannel[iclu] = cluster.channelVar();
        clusterDist[iclu] = cluster.dist();
        clusterVarDist[iclu] = cluster.distVar();
        clusterCovChannelDist[iclu] = cluster.channelDistCov();
        clusterTimestamp[iclu] = cluster.timestamp();
        clusterTimestampVar[iclu] = cluster.timestampVar();
        clusterValue[iclu] = cluster.value();
        clusterValueVar[iclu] = cluster.valueVar();
        clusterTrack[iclu] = cluster.isInTrack() ? cluster.track() : -1;
      }
      trees.clusters->Fill();
    }

    // local track states
    // if (trees.intercepts) {
    //   numIntercepts = 0;
    //   for (const auto& local : sensorEvent.localStates()) {
    //     if (kMaxTracks < static_cast<Index>(numIntercepts + 1))
    //       FAIL("intercepts exceed MAX_TRACKS");
    //     interceptU[numIntercepts] = local.loc0();
    //     interceptV[numIntercepts] = local.loc1();
    //     interceptSlopeU[numIntercepts] = local.slopeLoc0();
    //     interceptSlopeV[numIntercepts] = local.slopeLoc1();
    //     local.getCovSpatialPacked(interceptCov[numIntercepts]);
    //     interceptTrack[numIntercepts] = local.track();
    //     numIntercepts += 1;
    //   }
    //   trees.intercepts->Fill();
    // }
  }
  m_entries += 1;
}

} // namespace sfr

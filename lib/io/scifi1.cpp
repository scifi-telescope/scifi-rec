// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#include "scifi1.h"

#include <cassert>
#include <sys/stat.h>

#include "Compression.h"

#include "storage/event.h"
#include "utils/logger.h"

namespace sfr {

// -----------------------------------------------------------------------------
// common

SciFi1Common::SciFi1Common(RootFilePtr&& file)
    : m_file(std::move(file))
    , m_entries(0)
    , m_next(0)
    , m_eventInfo(nullptr)
    , m_tracks(nullptr)
{
}

// -----------------------------------------------------------------------------
// reader

int SciFi1Reader::check(const std::string& path, const toml::Value& cfg)
{
  struct stat info;
  int score = 0;

  // check if it is a regular file
  if (stat(path.c_str(), &info) != 0)
  {
    return 0;
  }
  else if ((info.st_mode & S_IFMT) == S_IFREG)
  {
    score += 50;
  }
  else { //exists, but not a regular file (e.g. directory)
    return 0;
  }

  std::unique_ptr<TFile> file(TFile::Open(path.c_str(), "READ"));
  if (!file)
    return 0;

  // should have an event tree, but is sometimes missing
  if (file->GetObjectUnchecked("event"))
    score += 50;
  // should have at least one sensor directory
  if (file->GetObjectUnchecked("sensor_0"))
    score += 50;
  return score;
}

std::shared_ptr<SciFi1Reader>
SciFi1Reader::open(const std::string& path,
                    const toml::Value& /* unused configuration */)
{
  return std::make_shared<SciFi1Reader>(path);
}

SciFi1Reader::SciFi1Reader(const std::string& path)
    : SciFi1Common(openRootRead(path.c_str()))
{
  int64_t entriesEvent = INT64_MAX;
  int64_t entriesTracks = INT64_MAX;

  // event tree is optional
  m_file->GetObject("event", m_eventInfo);
  if (m_eventInfo) {
    entriesEvent = m_eventInfo->GetEntriesFast();
    if (entriesEvent < 0)
      THROW("could not determine number of entries of Event tree");
    //m_eventInfo->SetBranchAddress("FrameNumber", &frameNumber);
    m_eventInfo->SetBranchAddress("timestamp", &triggerTimestamp);
    m_eventInfo->SetBranchAddress("flags", &triggerFlags);
    //m_eventInfo->SetBranchAddress("Invalid", &invalid);
  }

  // tracks tree is optional
  m_file->GetObject("Tracks", m_tracks);
  if (m_tracks) {
    entriesTracks = m_tracks->GetEntriesFast();
    if (entriesTracks < 0)
      THROW("could not determine number of entries in Tracks tree");
    m_tracks->SetBranchAddress("n_tracks", &numTracks);
    m_tracks->SetBranchAddress("chi2", trackChi2);
    m_tracks->SetBranchAddress("dof", trackDof);
    m_tracks->SetBranchAddress("x", trackX);
    m_tracks->SetBranchAddress("y", trackY);
    m_tracks->SetBranchAddress("slope_x", trackSlopeX);
    m_tracks->SetBranchAddress("slope_y", trackSlopeY);
    m_tracks->SetBranchAddress("cov", trackCov);
  }

  // entries from Events and Tracks. might still be undefined here
  m_entries = std::min(entriesEvent, entriesTracks);

  // per-sensor trees and finalize number of entries
  size_t numSensors = 0;
  while (true) {
    std::string name("sensor_" + std::to_string(numSensors));
    TDirectory* sensorDir = nullptr;
    m_file->GetObject(name.c_str(), sensorDir);
    if (!sensorDir)
      break;
    int64_t entriesSensor = addSensor(sensorDir);
    m_entries = std::min(m_entries, entriesSensor);
    numSensors += 1;
  }
  if (numSensors == 0)
    THROW("no sensors in '", path, "'");
  if (m_entries == INT64_MAX)
    THROW("could not determine number of events in '", path, "'");
  INFO("read ", numSensors, " sensors from '", path, "'");

  // NOTE 2017-10-25 msmk:
  //
  // having inconsistent entries between different sensors and the global
  // trees should be a fatal error. unfortunately, this can still happen for
  // valid data, e.g. for telescope data w/ manually synced trigger/busy-based
  // dut data or for independent Mimosa26 streams. To be able to handle these
  // we only report these cases as errrors here instead of failing altogether.

  // verify consistent number of entries between all trees
  if ((entriesEvent != INT64_MAX) && (entriesEvent != m_entries)) {
    WARN("Event tree has inconsistent entries=", entriesEvent,
         " expected=", m_entries);
  }
  if ((entriesTracks != INT64_MAX) && (entriesTracks != m_entries)) {
    WARN("Tracks tree has inconsistent entries=", entriesTracks,
         " expected=", m_entries);
  }
  for (size_t isensor = 0; isensor < m_sensors.size(); ++isensor) {
    if (m_sensors[isensor].entries != m_entries) {
      WARN("sensor ", isensor,
           " has inconsistent entries=", m_sensors[isensor].entries,
           " expected=", m_entries);
    }
  }
}

/** Setup trees for a new sensor and return the number of entries.
 *
 * Throws on inconsistent number of entries.
 */
int64_t SciFi1Reader::addSensor(TDirectory* dir)
{
  assert(dir && "Directory must be non-null");

  SensorTrees trees;
  // use INT64_MAX to mark uninitialized/ missing values that can be used
  // directly in std::min to find the number of entries
  int64_t entriesHits = INT64_MAX;
  int64_t entriesClusters = INT64_MAX;
  int64_t entriesIntercepts = INT64_MAX;

  dir->GetObject("hits", trees.hits);
  if (trees.hits) {
    entriesHits = trees.hits->GetEntriesFast();
    if (entriesHits < 0)
      THROW("could not determine entries in ", dir->GetName(), "/Hits tree");
    trees.hits->SetBranchAddress("n_hits", &numHits);
    trees.hits->SetBranchAddress("channel", hitChannel);
    trees.hits->SetBranchAddress("timestamp_coarse", hitTimestampCoarse);
    trees.hits->SetBranchAddress("timestamp_fine", hitTimestampFine);
    trees.hits->SetBranchAddress("value_coarse", hitValueCoarse);
    trees.hits->SetBranchAddress("value_fine", hitValueFine);
    trees.hits->SetBranchAddress("hit_in_cluster", hitInCluster);
  }
  dir->GetObject("clusters", trees.clusters);
  if (trees.clusters) {
    entriesClusters = trees.clusters->GetEntriesFast();
    if (entriesClusters < 0)
      THROW("could not determine entries in ", dir->GetName(),
            "/Clusters tree");
    trees.clusters->SetBranchAddress("n_clusters", &numClusters);
    trees.clusters->SetBranchAddress("channel", clusterChannel);
    trees.clusters->SetBranchAddress("var_channel", clusterVarChannel);
    trees.clusters->SetBranchAddress("dist", clusterDist);
    trees.clusters->SetBranchAddress("var_dist", clusterVarDist);
    trees.clusters->SetBranchAddress("cov_channel_dist", clusterCovChannelDist);
    trees.clusters->SetBranchAddress("timestamp", clusterTimestamp);
    trees.clusters->SetBranchAddress("timestamp_var", clusterTimestampVar);
    trees.clusters->SetBranchAddress("value", clusterValue);
    trees.clusters->SetBranchAddress("value_var", clusterValueVar);
    trees.clusters->SetBranchAddress("track", clusterTrack);
  }
  dir->GetObject("intercepts", trees.intercepts);
  if (trees.intercepts) {
    entriesIntercepts = trees.hits->GetEntriesFast();
    if (entriesIntercepts < 0)
      THROW("could not determine entries in ", dir->GetName(),
            "Intercepts tree");
    trees.intercepts->SetBranchAddress("n_intercepts", &numIntercepts);
    trees.intercepts->SetBranchAddress("u", interceptU);
    trees.intercepts->SetBranchAddress("v", interceptV);
    trees.intercepts->SetBranchAddress("slope_u", interceptSlopeU);
    trees.intercepts->SetBranchAddress("slope_v", interceptSlopeV);
    trees.intercepts->SetBranchAddress("cov", interceptCov);
    trees.intercepts->SetBranchAddress("track", interceptTrack);
  }

  // this directory does not contain any valid data
  if ((entriesHits == INT64_MAX) && (entriesClusters == INT64_MAX) &&
      (entriesIntercepts == INT64_MAX))
    THROW("could not find any of ", dir->GetName(),
          "/{Hits,Clusters,Intercepts}");

  // check that all active trees have consistent entries
  trees.entries = std::min({entriesHits, entriesClusters, entriesIntercepts});
  if ((entriesHits != INT64_MAX) && (entriesHits != trees.entries))
    THROW("inconsistent entries in ", dir->GetName(),
          "/Hits tree entries=", entriesHits, " expected=", trees.entries);
  if ((entriesClusters != INT64_MAX) && (entriesClusters != trees.entries))
    THROW("inconsistent entries in ", dir->GetName(),
          "/Clusters tree entries=", entriesClusters,
          " expected=", trees.entries);
  if ((entriesIntercepts != INT64_MAX) && (entriesIntercepts != trees.entries))
    THROW("inconsistent entries in ", dir->GetName(),
          "/Intercepts tree entries=", entriesIntercepts,
          " expected=", trees.entries);

  m_sensors.push_back(trees);
  return trees.entries;
}

std::string SciFi1Reader::name() const { return "SciFi1Reader"; }

uint64_t SciFi1Reader::numEvents() const
{
  return static_cast<uint64_t>(m_entries);
}

size_t SciFi1Reader::numSensors() const { return m_sensors.size(); }

void SciFi1Reader::skip(uint64_t n)
{
  if (m_entries <= static_cast<int64_t>(m_next + n)) {
    WARN("skipping ", n, " events goes beyond available events");
    m_next = m_entries;
  } else {
    m_next += n;
  }
}

bool SciFi1Reader::read(Event& event)
{
  /* Note: fill in reversed order: tracks first, hits last. This is so that
   * once a hit is produced, it can immediately recieve the address of its
   * parent cluster, likewise for clusters and track. */
  if (m_entries <= m_next)
    return false;

  int64_t ievent = m_next++;

  // global event data
  if (m_eventInfo) {
    if (m_eventInfo->GetEntry(ievent) <= 0)
      FAIL("could not read 'Events' entry ", ievent);
    // listen chap, here's the deal:
    // we want a timestamp, i.e. a simple counter of clockcycles or bunch
    // crossings, for each event that defines the trigger/ readout time with
    // the highest possible precision. Unfortunately, the RCE ROOT output format
    // has stupid names. The `TimeStamp` branch stores the Unix-`timestamp`
    // (number of seconds since 01.01.1970) of the point in time when the event
    // was written to disk. This might or might not have a constant correlation
    // to the actual trigger time and has only a 1s resolution, i.e. it is
    // completely useless. The `TriggerTime` actually stores the internal
    // FPGA timestamp/ clock cyles and is what we need to use.
    event.clear(triggerFlags, triggerTimestamp);
  } else {
    event.clear(ievent);
  }

  // global tracks info
  if (m_tracks) {
    // if (m_tracks->GetEntry(ievent) <= 0)
    //   FAIL("could not read 'Tracks' entry ", ievent);
    // for (Int_t itrack = 0; itrack < numTracks; ++itrack) {
    //   TrackState state(trackX[itrack], trackY[itrack], trackSlopeX[itrack],
    //                    trackSlopeY[itrack]);
    //   state.setCovSpatialPacked(trackCov[itrack]);
    //   event.addTrack({state, trackChi2[itrack], trackDof[itrack]});
    // }
  }

  // per-sensor data
  for (size_t isensor = 0; isensor < numSensors(); ++isensor) {
    SensorTrees& trees = m_sensors[isensor];
    SensorEvent& sensorEvent = event.getSensorEvent(isensor);

    // local track states
    if (trees.intercepts) {
      // if (trees.intercepts->GetEntry(ievent) <= 0)
      //   FAIL("could not read 'Intercepts' entry ", ievent);

      // for (Int_t iintercept = 0; iintercept < numIntercepts; ++iintercept) {
      //   TrackState local(interceptU[iintercept], interceptV[iintercept],
      //                    interceptSlopeU[iintercept],
      //                    interceptSlopeV[iintercept]);
      //   local.setCovSpatialPacked(interceptCov[iintercept]);
      //   sensorEvent.setLocalState(interceptTrack[iintercept], local);
      // }
    }

    // local clusters
    if (trees.clusters) {
      if (trees.clusters->GetEntry(ievent) <= 0)
        FAIL("could not read 'Clusters' entry ", ievent);

      for (Int_t icluster = 0; icluster < numClusters; ++icluster) {
        // Cluster& cluster = sensorEvent.addCluster(
        sensorEvent.addCluster(
            clusterChannel[icluster], clusterVarChannel[icluster],
            clusterDist[icluster], clusterVarDist[icluster],
            clusterCovChannelDist[icluster],
            clusterTimestamp[icluster], clusterTimestampVar[icluster],
            clusterValue[icluster], clusterValueVar[icluster]);
        // Fix cluster/track relationship if possible
        // if (m_tracks && (0 <= clusterTrack[icluster])) {
        //   cluster.setTrack(clusterTrack[icluster]);
        //   event.getTrack(clusterTrack[icluster]).addCluster(isensor, icluster);
        // }
      }
    }

    // local hits
    if (trees.hits) {
      if (trees.hits->GetEntry(ievent) <= 0)
        FAIL("could not read 'Hits' entry ", ievent);

      for (Int_t ihit = 0; ihit < numHits; ++ihit) {
        Hit& hit = sensorEvent.addHit(hitChannel[ihit], hitTimestampCoarse[ihit], hitTimestampFine[ihit], hitValueCoarse[ihit], hitValueFine[ihit], 0);
        // Fix hit/cluster relationship is possibl
        if (trees.clusters && hitInCluster[ihit] >= 0)
          sensorEvent.getCluster(hitInCluster[ihit]).addHit(hit);
      }
    }
  } // end loop in sensor_s
  return true;
}

// -----------------------------------------------------------------------------
// writer

SciFi1Writer::SciFi1Writer(const std::string& path, size_t numSensors)
    : SciFi1Common(openRootWrite(path))
{
  m_file->cd();

  // global event tree
  m_eventInfo = new TTree("event", "Event information");
  m_eventInfo->SetDirectory(m_file.get());
  m_eventInfo->Branch("timestamp", &triggerTimestamp, "Timestamp/l");
  m_eventInfo->Branch("flags", &triggerFlags, "Flags/l");

  // global track tree
  m_tracks = new TTree("tracks", "Track parameters");
  m_tracks->SetDirectory(m_file.get());
  m_tracks->Branch("n_tracks", &numTracks, "NTracks/I");
  m_tracks->Branch("chi2", trackChi2, "Chi2[NTracks]/D");
  m_tracks->Branch("dof", trackDof, "Dof[NTracks]/I");
  m_tracks->Branch("x", trackX, "X[NTracks]/D");
  m_tracks->Branch("y", trackY, "Y[NTracks]/D");
  m_tracks->Branch("slope_x", trackSlopeX, "SlopeX[NTracks]/D");
  m_tracks->Branch("slope_y", trackSlopeY, "SlopeY[NTracks]/D");
  m_tracks->Branch("cov", trackCov, "Cov[NTracks][10]/D");

  // per-sensor trees
  for (size_t isensor = 0; isensor < numSensors; ++isensor) {
    std::string name("sensor_" + std::to_string(isensor));
    TDirectory* sensorDir = m_file->mkdir(name.c_str());
    addSensor(sensorDir);
  }
}

void SciFi1Writer::addSensor(TDirectory* dir)
{
  dir->cd();

  SensorTrees trees;
  // local hits
  trees.hits = new TTree("hits", "hits");
  trees.hits->SetDirectory(dir);
  trees.hits->Branch("n_hits", &numHits, "NHits/I");
  trees.hits->Branch("channel", hitChannel, "Channel[NHits]/I");
  trees.hits->Branch("timestamp_coarse", hitTimestampCoarse, "HitTimestampCoarse[NHits]/L");
  trees.hits->Branch("timestamp_fine", hitTimestampFine, "HitTimestampFine[NHits]/L");
  trees.hits->Branch("value_coarse", hitValueCoarse, "HitValueCoarse[NHits]/L");
  trees.hits->Branch("value_fine", hitValueFine, "HitValueFine[NHits]/L");
  trees.hits->Branch("hit_in_cluster", hitInCluster, "HitInCluster[NHits]/I");
  // local clusters
  trees.clusters = new TTree("clusters", "Clusters");
  trees.clusters->SetDirectory(dir);
  trees.clusters->Branch("n_clusters", &numClusters, "NClusters/I");
  trees.clusters->Branch("channel", clusterChannel, "Channel[NClusters]/D");
  trees.clusters->Branch("var_channel", clusterVarChannel, "VarChannel[NClusters]/D");
  trees.clusters->Branch("dist", clusterDist, "Dist[NClusters]/D");
  trees.clusters->Branch("var_dist", clusterVarDist, "VarDist[NClusters]/D");
  trees.clusters->Branch("cov_channel_dist", clusterCovChannelDist,
                         "CovChannelDist[NClusters]/D");
  trees.clusters->Branch("timestamp", clusterTimestamp, "Timestamp[NClusters]/D");
  trees.clusters->Branch("timestamp_var", clusterTimestampVar, "TimestampVar[NClusters]/D");
  trees.clusters->Branch("value", clusterValue, "Value[NClusters]/D");
  trees.clusters->Branch("value_var", clusterValueVar, "ValueVar[NClusters]/D");
  trees.clusters->Branch("track", clusterTrack, "Track[NClusters]/I");
  // local track states
  trees.intercepts = new TTree("intercepts", "Intercepts");
  trees.intercepts->SetDirectory(dir);
  trees.intercepts->Branch("n_intercepts", &numIntercepts, "NIntercepts/I");
  trees.intercepts->Branch("u", interceptU, "U[NIntercepts]/D");
  trees.intercepts->Branch("v", interceptV, "V[NIntercepts]/D");
  trees.intercepts->Branch("slope_u", interceptSlopeU, "SlopeU[NIntercepts]/D");
  trees.intercepts->Branch("slope_v", interceptSlopeV, "SlopeV[NIntercepts]/D");
  trees.intercepts->Branch("cov", interceptCov, "Cov[NIntercepts][10]/D");
  trees.intercepts->Branch("track", interceptTrack, "Track[NIntercepts]/I");
  m_sensors.emplace_back(trees);
}

SciFi1Writer::~SciFi1Writer()
{
  if (m_file) {
    INFO("wrote ", m_sensors.size(), " sensors to '", m_file->GetPath(), "'");
  }
}

std::string SciFi1Writer::name() const { return "SciFi1Writer"; }

void SciFi1Writer::append(const Event& event)
{
  if (event.numSensorEvents() != m_sensors.size())
    FAIL("inconsistent sensors numbers. events has ", event.numSensorEvents(),
         ", but the writer expected ", m_sensors.size());

  // global event info is **always** filled
  triggerTimestamp = event.timestamp();
  triggerFlags = event.flags();
  m_eventInfo->Fill();

  // tracks
  // if (m_tracks) {
  //   if (kMaxTracks < event.numTracks())
  //     FAIL("tracks exceed MAX_TRACKS");
  //   numTracks = event.numTracks();
  //   for (Index itrack = 0; itrack < event.numTracks(); ++itrack) {
  //     const Track& track = event.getTrack(itrack);
  //     trackChi2[itrack] = track.chi2();
  //     trackDof[itrack] = track.degreesOfFreedom();
  //     const TrackState& state = track.globalState();
  //     trackX[itrack] = state.loc0();
  //     trackY[itrack] = state.loc1();
  //     trackSlopeX[itrack] = state.slopeLoc0();
  //     trackSlopeY[itrack] = state.slopeLoc1();
  //     state.getCovSpatialPacked(trackCov[itrack]);
  //   }
  //   m_tracks->Fill();
  // }

  // per-sensor data
  for (Index isensor = 0; isensor < event.numSensorEvents(); ++isensor) {
    SensorTrees& trees = m_sensors[isensor];
    const auto& sensorEvent = event.getSensorEvent(isensor);

    // local hits
    if (trees.hits) {
      if (kMaxHits < sensorEvent.numHits())
        FAIL("hits exceed MAX_HITS");
      numHits = sensorEvent.numHits();
      for (Index ihit = 0; ihit < sensorEvent.numHits(); ++ihit) {
        const Hit hit = sensorEvent.getHit(ihit);
        hitChannel[ihit] = hit.channel();
        hitTimestampCoarse[ihit] = hit.coarseTimestamp();
        hitTimestampFine[ihit] = hit.fineTimestamp();
        hitValueCoarse[ihit] = hit.coarseValue();
        hitValueFine[ihit] = hit.fineValue();
        hitInCluster[ihit] = hit.isInCluster() ? hit.cluster() : -1;
      }
      trees.hits->Fill();
    }

    // local clusters
    if (trees.clusters) {
      if (kMaxHits < sensorEvent.numClusters())
        FAIL("clusters exceed MAX_HITS");
      numClusters = sensorEvent.numClusters();
      for (Index iclu = 0; iclu < sensorEvent.numClusters(); ++iclu) {
        const Cluster& cluster = sensorEvent.getCluster(iclu);
        clusterChannel[iclu] = cluster.channel();
        clusterVarChannel[iclu] = cluster.channelVar();
        clusterDist[iclu] = cluster.dist();
        clusterVarDist[iclu] = cluster.distVar();
        clusterCovChannelDist[iclu] = cluster.channelDistCov();
        clusterTimestamp[iclu] = cluster.timestamp();
        clusterTimestampVar[iclu] = cluster.timestampVar();
        clusterValue[iclu] = cluster.value();
        clusterValueVar[iclu] = cluster.valueVar();
        clusterTrack[iclu] = cluster.isInTrack() ? cluster.track() : -1;
      }
      trees.clusters->Fill();
    }

    // local track states
    // if (trees.intercepts) {
    //   numIntercepts = 0;
    //   for (const auto& local : sensorEvent.localStates()) {
    //     if (kMaxTracks < static_cast<Index>(numIntercepts + 1))
    //       FAIL("intercepts exceed MAX_TRACKS");
    //     interceptU[numIntercepts] = local.loc0();
    //     interceptV[numIntercepts] = local.loc1();
    //     interceptSlopeU[numIntercepts] = local.slopeLoc0();
    //     interceptSlopeV[numIntercepts] = local.slopeLoc1();
    //     local.getCovSpatialPacked(interceptCov[numIntercepts]);
    //     interceptTrack[numIntercepts] = local.track();
    //     numIntercepts += 1;
    //   }
    //   trees.intercepts->Fill();
    // }
  }
  m_entries += 1;
}

} // namespace sfr

// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

/**
 * \author Moritz Kiehn <msmk@cern.ch>
 * \date 2016-08-24
 */

#pragma once

#include <string>

namespace sfr {

class Event;

/** Interface for algorithms to process and modify events. */
class Processor {
public:
  virtual ~Processor() = default;
  virtual std::string name() const = 0;
  virtual void execute(Event&) const = 0;
};

} // namespace sfr

// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#include "utils/logger.h"
#include "utils/application.h"
#include "utils/root.h"
#include "io/scifi1.h"
#include "analyzers/hits.h"
#include "processors/applycalibration.h"

using namespace sfr;

int main(int argc, const char** argv) {
  toml::Table defaults = {
    {"data_filename_template", "raw_%02d.txt"},
    {"split_events", false},
  };

  Application app("convert", "Converts raw data", defaults);
  app.initialize(argc, argv);

  auto hists = openRootWrite(app.outputPath("-hists.root"));

  const auto& dev = app.device();

  EventLoop loop = app.makeEventLoop();
  
  // setup calibration application
  for (Index id : dev.sensorIds()) {
    const Sensor& sensor = dev.getSensor(id);
    try {
      switch (sensor.measurement()) {
        case Sensor::Measurement::SciFiStic:
        case Sensor::Measurement::SciFiSticShower:
          loop.addSensorProcessor(id, std::make_shared<SticApplyCalibration>(sensor, app.calibrationFilename(id)));
          break;
        default:
          break;
      }
    }
    catch (const std::runtime_error& e) { // if the -m option is not used, just give a warning
      WARN(e.what());
      break;
    }
  }

  loop.addAnalyzer(std::make_shared<Hits>(hists.get(), app.device()));
  loop.addWriter(std::make_shared<SciFi1Writer>(app.outputPath(".root"), dev.numSensors()));
  loop.run();

  return 0;

}
# Copyright (c) 2020 The SciFi reconstruction authors
# SPDX-License-Identifier: MIT

# define a function that describes how to compile each tool
function(add_tool name)
  # set executable name
  set(_exe "sfr-${name}")
  # add the executable to the things that will be compiled
  # ARGN is basically name.cpp (represents the arguments after "name")
  add_executable(${_exe} ${ARGN})
  # link the libraries
  target_link_libraries(${_exe} sfr)
  # install in bin
  install(TARGETS ${_exe} RUNTIME DESTINATION bin)
endfunction()

add_tool(convert convert.cpp)
add_tool(calibrate calibrate.cpp)
add_tool(reconstruct reconstruct.cpp)
add_tool(align align.cpp)

// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#include <climits>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <sys/stat.h>

#include "utils/logger.h"
#include "utils/application.h"
#include "utils/root.h"
#include "utils/definitions.h"
#include "analyzers/calibration.h"
#include "analyzers/hits.h"
#include "loop/eventloop.h"
#include "processors/hitfilter.h"


using namespace sfr;

int main(int argc, const char** argv) {
    toml::Table defaults = {
      {"data_filename_template", "raw_%02d.txt"},
      {"calib_filename_template", "calib_%02d.txt"},
      {"channel_coefficient", 0.},
  };
    Application app("calibrate", "Calculate time calibration", defaults);
    app.initialize(argc, argv);

    // check if the given path is a directory
    struct stat info;
    if (stat(app.outputPath("").c_str(), &info) != 0)
    {
      FAIL(app.outputPath(""), " does not exist");
    }
    else if (!(info.st_mode & S_IFDIR))
    {
      FAIL(app.outputPath(""), " is not a directory");
    }

    auto hists = openRootWrite(app.outputPath("/calibration-hists.root"));

    const auto& cfg = app.config();
    const auto& dev = app.device();

    EventLoop loop = app.makeEventLoop();

    std::vector<std::shared_ptr<SticCalibration>> calAnalizers;
    auto sensorIds = cfg.get<std::vector<Index>>("sensor_ids");

    loop.addAnalyzer(std::make_shared<Hits>(hists.get(), dev));
    
    for (Index id : sensorIds) {
      const Sensor& sensor = dev.getSensor(id);
      switch (sensor.measurement()) {
      case Sensor::Measurement::SciFiStic:
      case Sensor::Measurement::SciFiSticShower:
        loop.addSensorProcessor(id, std::make_shared<SticHitFilter>(sensor));
        calAnalizers.push_back(std::make_shared<SticCalibration>( hists.get(), sensor, cfg.get<Scalar>("channel_coefficient") ));
        loop.addAnalyzer(calAnalizers.back());
        break;
      default:
        break;
      }
      
    }

    loop.run();

    for (auto c : calAnalizers) {
      std::string filename = stringFormat(cfg.get<std::string>("calib_filename_template"), c->getSensorId());
      DEBUG("Saving calibration file ", filename);
      c->getSensorCalibration().writeFile(app.outputPath("/") + filename);
    }

    return 0;

}
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#include "utils/logger.h"
#include "utils/application.h"
#include "utils/root.h"
#include "utils/definitions.h"
#include "processors/clusterizer.h"
#include "processors/setupsensors.h"
#include "loop/eventloop.h"
#include "io/scifi1.h"
#include "io/scifi1clu.h"

#include "analyzers/hits.h"
#include "analyzers/clusters.h"

using namespace sfr;

int main(int argc, const char** argv) {

  Application app("reconstruct", "Run reconstruction", toml::Table());
  app.initialize(argc, argv);

  auto hists = openRootWrite(app.outputPath("-hists.root"));

  auto loop = app.makeEventLoop();
  const auto& dev = app.device();

  // local per-sensor processing
  setupPerSensorProcessing(app.device(), loop);
  loop.addAnalyzer(std::make_shared<Hits>(hists.get(), dev));
  loop.addAnalyzer(std::make_shared<Clusters>(hists.get(), dev));
  loop.addWriter(std::make_shared<SciFi1Writer>(app.outputPath(".root"), dev.numSensors()));
  loop.addWriter(std::make_shared<SciFi1CluWriter>(app.outputPath("-clusters.root"), dev));

  loop.run();

  return 0;
}